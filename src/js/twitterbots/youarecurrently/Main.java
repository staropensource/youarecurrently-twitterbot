package js.twitterbots.youarecurrently;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.RequestToken;
import twitter4j.auth.AccessToken;

public class Main {
	
	// Empty/filled variables, used while executing
	protected static String consumerkey = "<Consumer key value>";
	protected static String consumersecret = "<Consumer secret value>";
	protected static String pathToAccessTokenFile = System.getProperty("user.home") + System.getProperty("file.separator") + ".youarecurrentlybot.atoken";
	protected static String pathToAccessTokenSecretFile = System.getProperty("user.home") + System.getProperty("file.separator") + ".youarecurrentlybot.atokensecret";
	protected static Twitter t4j = null; // Variable for Twitter4J
	protected static RequestToken t4jrtoken = null; // Request token
	protected static AccessToken t4jatoken = null; // Access token
	protected static String t4jpin = null; // Pin
	protected static BufferedReader bufferreader = null; // Buffer reader
	protected static Status t4jstatus = null; // The sender of the tweet
	protected static String randomStatus = null; // The random status
	protected static String[] wordsToUse = { "word 1", "word 2", "word 3" }; // The words that the bot randomly chooses
	protected static int waitToTweet = 1800000; // The time in ms that the bot waits until it sends another tweet. Make sure it's low, otherwise you get rate-limited by Twitters API!
	protected static int twitterConnectionError = 30000; // The time in ms that the bot waits if it fails to send a tweet and retries
	protected static boolean runQuiet = false; // Defines if the bot should show a message that it tweeted successfully and what message
	protected static boolean aboutInfo = false; // Defines if the bot should tweet a info message.
	
	public static void main(String[] args) {
		// Startup method
		if(args.length > 0) {
			if(args[0].toLowerCase().equals("help")) {
				System.out.print( "argument      | action\n"
								+ "help          | shows this help screen\n"
								+ "quiet         | starts the bot without printing if the tweet was successfull and what it generated\n"
								+ "generate      | generates the login data and saves it to the harddisk in order for the bot to work\n"
								+ "about         | get a little bit of information about this bot\n"
								+ "abouttweet    | tweet a about message\n"
								+ "<no args>     | starts the bot normally\n");
				System.exit(0);
			} else if(args[0].toLowerCase().equals("quiet")) {
				System.out.print("Running bot with argument 'quiet'.\n");
				runQuiet = true;
			} else if(args[0].toLowerCase().equals("generate")) {
				generate();
			} else if(args[0].toLowerCase().equals("about")) {
				System.out.print("This bot was originally created by JeremyStar/JeremyStarTM (@JeremyStarTM on Twitter) and tweets 'You are currently <word(s)> every twenty minutes.\nSource code:");
			} else if(args[0].toLowerCase().equals("abouttweet")) {
				aboutInfo = true;
			} else {
				runQuiet = false;
			}
		}
		t4j = TwitterFactory.getSingleton();
		bufferreader = new BufferedReader(new InputStreamReader(System.in));
		String accessToken = null;
		String accessTokenSecret = null;
		if(!runQuiet) {
			System.out.print("Loading access token from disk to memory...\nWARNING: If you get a crash, it's because the accesstoken isn't generated yet, so try appending the argument 'generate' to generate it.\n");
		}
		try(BufferedReader bufferedReader2 = new BufferedReader(new FileReader(pathToAccessTokenFile))) {
		     accessToken = bufferedReader2.readLine().toString();
		     bufferedReader2.close();
		} catch (FileNotFoundException e) {
		    crash(false, true, e.getStackTrace().toString(), 0, false);
		} catch (IOException e) {
		    crash(false, true, e.getStackTrace().toString(), 0, false);
		}
		try(BufferedReader bufferedReader2 = new BufferedReader(new FileReader(pathToAccessTokenSecretFile))) {
		     accessTokenSecret = bufferedReader2.readLine().toString();
		     bufferedReader2.close();
		} catch (FileNotFoundException e) {
		    crash(false, true, e.getStackTrace().toString(), 0, false);
		} catch (IOException e) {
		    crash(false, true, e.getStackTrace().toString(), 0, false);
		}
		t4jatoken = new AccessToken(accessToken, accessTokenSecret);
		t4j.setOAuthConsumer(consumerkey, consumersecret);
		t4j.setOAuthAccessToken(t4jatoken);
		try {
			t4jstatus = t4j.updateStatus(randomStatus);
		} catch (TwitterException te) {
			crash(true, true, te.getStackTrace().toString(), te.getStatusCode(), true);
		}
		tweet();
		
	}
	
	protected static void tweet() {
		// Tweeting and randomizing function
		if(aboutInfo) {
			randomStatus =	"@YouAreCurrently is a bot made by @JeremyStarTM.\n"
						+	"It tweets \"You are currently <word(s)>\" every twenty minutes from a random list.\n"
						+	"You can see the source code on https://gitlab.com/staropensource/youarecurrently-twitterbot.\n"
						+	"The @YouAreCurrently bot is written in Java 11.";
		} else {
			if(randomStatus == null) {
				int valueWord = (int) (Math.random()*(wordsToUse.length - 0));
				randomStatus = "You are currently " + wordsToUse[valueWord];
			}
		}
		try {
			t4jstatus = t4j.updateStatus(randomStatus);
		} catch (TwitterException te) {
			crash(true, true, te.getStackTrace().toString(), te.getStatusCode(), true);
		}
		if(!runQuiet) {
			System.out.print("\nSend tweet with message '" + randomStatus + "' to Twitter successfully.\n");
		}
		if(!aboutInfo) {
			try {
				Thread.sleep(waitToTweet);
			} catch (InterruptedException pe) {
				crash(false, true, pe.getStackTrace().toString(), 0, true);
			}
			randomStatus = null;
			tweet();
		} else {
			System.exit(0);
		}
	}
	
	protected static void generate() {
		t4j = TwitterFactory.getSingleton();
		bufferreader = new BufferedReader(new InputStreamReader(System.in));
		t4j.setOAuthConsumer(consumerkey, consumersecret);
		try {
			t4jrtoken = t4j.getOAuthRequestToken();
		} catch (TwitterException te) {
			crash(true, true, te.getStackTrace().toString(), te.getStatusCode(), false);
		}
		while(t4jatoken == null) {
			System.out.print("Access URL:\n");
			System.out.print(t4jrtoken.getAuthorizationURL() + "\n");
			System.out.print("\nPIN:");
			try {
				t4jpin = bufferreader.readLine();
			} catch (IOException pe) {
				crash(false, true, pe.getStackTrace().toString(), 0, false);
			}
			try {
				if(t4jpin.length() > 0) {
					t4jatoken = t4j.getOAuthAccessToken(t4jrtoken, t4jpin);
				} else {
					t4jatoken = t4j.getOAuthAccessToken();
				}
			} catch (TwitterException te) {
				crash(false, true, te.getStackTrace().toString(), te.getStatusCode(), false);
			}
		}
		try(FileOutputStream writeToDisk = new FileOutputStream(pathToAccessTokenFile)) {
			writeToDisk.write(t4jatoken.getToken().toString().getBytes());
		} catch (FileNotFoundException e) {
		    crash(false, true, e.getStackTrace().toString(), 0, false);
		} catch (IOException e) {
		    crash(false, true, e.getStackTrace().toString(), 0, false);
		}
		try(FileOutputStream writeToDisk = new FileOutputStream(pathToAccessTokenSecretFile)) {
			writeToDisk.write(t4jatoken.getTokenSecret().toString().getBytes());
		} catch (FileNotFoundException e) {
		    crash(false, true, e.getStackTrace().toString(), 0, false);
		} catch (IOException e) {
		    crash(false, true, e.getStackTrace().toString(), 0, false);
		}
		System.out.print("Generated bot login data successfully, you can now start the bot.\n");
		System.exit(0);
	}
	
	protected static void crash(boolean t4jfault, boolean stacktrace, String stackTraceElements, int statusCode, boolean returnedInTweetFunction) {
		// Crash handler
		if(t4jfault) {
			if(stacktrace) {
				System.out.print("Twitter4J returned an error\nStatus code: " + statusCode + "\nHere's stacktrace:\n" + stackTraceElements.toString() + "\n");
			} else {
				System.out.print("Twitter4J returned an error\nStatus code: " + statusCode + "\nNo) stacktrace." + "\n");
			}
		} else {
			if(stacktrace) {
				System.out.print("YACB returned an error\nStatus code: " + statusCode + "\nHere's stacktrace:\n" + stackTraceElements.toString() + "\n");
			} else {
				System.out.print("YACB returned an error\nStatus code: " + statusCode + "\nNo stacktrace." + "\n");
			}
		}
		if(returnedInTweetFunction) {
			try {
				Thread.sleep(twitterConnectionError);
			} catch (InterruptedException pe) {
				crash(false, true, pe.getStackTrace().toString(), 0, true);
			}
			tweet();
		} else {
			System.exit(1);
		}
	}
}
