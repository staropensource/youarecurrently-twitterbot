![Twitter4J powered badge](https://twitter4j.org/en/images/powered-by-badge/powered-by-twitter4j-138x30.png)

# What is the You are currently Twitter bot?
The You are currently Twitter bot is, well, a Twitter bot. It tweets "You are currently <word(s)>" every twenty minutes.

# Downloads
There are no downloads, since you need a Twitter API Consumer Key and a Twitter API Consumer Secret, you'll need to build it yourself.

# How to build/use it?
**TODO: Add working build/use implementation**

# In which language is apt-frontend written?
The You are currently bot is writtten in Java.

# Does this project have a license?
Yes, we have a license. You can find it at [LICENSE](https://gitlab.com/staropensource/youarecurrently-twitterbot/-/blob/master/LICENSE).

# How to contribute?
You can contribute by:
 * Making useful issues
 * Fixing bugs
